import json
import logging

from src.AI.ai_controller import AIController


class BazingaBot(object):

    def __init__(self, socket, name, key):
        logging.info("Starting BazingaBot...")
        logging.info("Build 8")
        self.socket = socket
        self.name = name
        self.key = key
        self.track = None
        self.ai_controller = None

    def msg(self, msg_type, data, gametick=None):
        if gametick:
            self.send(json.dumps({"msgType": msg_type,
                                  "data": data,
                                  "gameTick" : gametick}))
        else:
            self.send(json.dumps({"msgType": msg_type,
                                  "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, track, car_count):
        botId = {"name": self.name,
                 "key": self.key}
        return self.msg("joinRace", {"botId"     : botId,
                                     "trackName" : track,
                                     "carCount"  : car_count})

    def create(self, track_name, password, car_count):
        botId = {"name": self.name,
                 "key": self.key}
        return self.msg("createRace", {"botId": botId,
                                       "trackName": track_name,
                                       "password": password,
                                       "carCount": car_count})

    def throttle(self, throttle, game_tick):
        logging.info("Throttle: {0}".format(throttle))
        self.msg("throttle", throttle, game_tick)

    def apply_pending_switch(self, game_tick):
        switch = self.ai_controller.lane_switcher.pending_switch
        if switch:
             logging.info("Switching to {}".format(switch))
             self.msg("switchLane", switch, game_tick)
             return True
        return False

    def apply_pending_turbo(self, game_tick):
        turbo = self.ai_controller.turbo_ai.pending_turbo
        if turbo:
            self.msg("turbo", turbo, game_tick)
            return True
        return False

    def ping(self, game_tick=None):
        self.msg("ping", {}, game_tick)

    def run(self):
        # self.join_race("germany", 1)
        self.join()
        self.msg_loop()

    #region Callbacks
    def on_join(self, data):
        logging.info("Joined")
        self.ping()

    def on_game_start(self, data):
        logging.info("Race started")
        self.ping()

    def on_car_positions(self, data, game_tick):
        self.ai_controller.update(data, game_tick)
        if self.apply_pending_switch(game_tick):
            return
        if self.apply_pending_turbo(game_tick):
            return
        self.throttle(self.ai_controller.throttle,
                      game_tick)

    def on_crash(self, data):
        logging.info("Someone crashed")
        self.ai_controller.crashed(data)
        self.ping()

    def on_spawn(self, data):
        logging.info("Someone spawned")
        self.ai_controller.spawned(data)
        self.ping()

    def on_game_end(self, data):
        logging.info("Game ended")
        self.ai_controller.on_game_end()
        self.ping()

    def on_error(self, data):
        logging.error("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        logging.info("Game Init")
        self.ai_controller.set_race(data["race"])
        self.ping()

    def on_your_car(self, data):
        logging.info("Your Car")
        self.ai_controller = AIController(data)
        self.ping()

    def on_tournament_end(self, data):
        logging.info("Tournament end")
        self.ping()

    def on_lap_finished(self, data, game_tick):
        logging.info("Lap finished")
        self.ai_controller.race_model.update_lap(data)

    def on_turbo_available(self, data):
        logging.info("Turbo Available")
        self.ai_controller.turbo_ai.append_new_turbo(data)

    def on_turbo_start(self, data):
        self.ai_controller.turbo_ai.on_turbo_start(data)

    def on_turbo_end(self, data):
        self.ai_controller.turbo_ai.on_turbo_stop(data)

    #endregion

    def msg_loop(self):
        msg_map = {
            # Value is Tuple (method, has_game_tick)
            'join': (self.on_join, False),
            'gameStart': (self.on_game_start, False),
            'gameInit': (self.on_game_init, False),
            'yourCar': (self.on_your_car, False),
            'carPositions': (self.on_car_positions, True),
            'lapFinished': (self.on_lap_finished, True),
            'crash': (self.on_crash, False),
            'spawn': (self.on_spawn, False),
            'gameEnd': (self.on_game_end, False),
            'tournamentEnd' : (self.on_tournament_end, False),
            'error': (self.on_error, False),
            'turboAvailable': (self.on_turbo_available, False),
            'turboStart': (self.on_turbo_start, False),
            'turboEnd': (self.on_turbo_end, False),
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        try:
            while line:
                msg = json.loads(line)
                msg_type, data = msg['msgType'], msg['data']
                if msg_type in msg_map:
                    if msg_map[msg_type][1]:
                        game_tick = int(msg.get("gameTick", -1))
                        msg_map[msg_type][0](data, game_tick)
                    else:
                        msg_map[msg_type][0](data)
                else:
                    logging.info("Got {0}".format(msg_type))
                    self.ping()
                line = socket_file.readline()
        finally:
            # Save data if available
            if self.ai_controller:
                self.ai_controller.on_finish()
                # Export parameters to database
                if self.ai_controller.race_data:
                    self.ai_controller.race_data.export()
                    self.ai_controller.race_data.close()

