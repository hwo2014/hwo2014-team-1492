
def seq_in_seq(subseq, seq):
    """
    Brute force implementation for determining if given
    list is a sublist of another list
    """
    while subseq[0] in seq:
        index = seq.index(subseq[0])
        if subseq == seq[index:index + len(subseq)]:
            return index
        else:
            seq = seq[index + 1:]
    else:
        return -1


def abs_list(l):
    l1 = []
    for e in l:
        l1.append(abs(e))
    return l1


