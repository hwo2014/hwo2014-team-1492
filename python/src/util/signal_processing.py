

def linear_regression(y, x=None):
    assert isinstance(y, list)
    if not x:
        x = range(len(y))
    assert isinstance(x, list)
    assert len(x) == len(y)
    assert len(x) > 1

    data_length = len(x)

    mean_x = mean(x)
    mean_y = mean(y)
    mean_x_squared = mean([a*a for a in x])
    covariance = mean([x[i] * y[i] for i in range(data_length)]) - mean_x * mean_y

    k = covariance / (mean_x_squared - mean_x**2)
    d = mean_y - k * mean_x

    return d, k


def diff(l):
    assert isinstance(l, list)
    if len(l) < 2:
        return []

    prev = l[0]
    b = []
    for i in l[1:]:
        b.append(i - prev)
        prev = i
    return b


def mean(l):
    return float(sum(l))/len(l) if len(l) > 0 else float('nan')


def variance(l):
    if len(l) < 2:
        return float('nan')
    v = 0
    m = mean(l)
    for e in l:
        v += ((e - m) ** 2)
    v /= (len(l) - 1)
    return v


def median(l):
    assert len(l) > 0

    srtd = sorted(l)
    mid = len(l) / 2

    if len(l) % 2 == 0:
        return (srtd[mid-1] + srtd[mid]) / 2.0
    else:
        return srtd[mid]


def find_element(operator, x):
    val = operator(x)
    idx = [i for i, j in enumerate(x) if j == val][0]
    return val, idx

