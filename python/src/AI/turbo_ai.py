
import logging
from src.racemodel.track_segment import TrackSegment


class TurboAI(object):

    turbo_msg = "Pow pow pow pow pow"
    ACCEPT_TURBO, DO_NOT_ACCEPT = range(2)

    def __init__(self, car_id, race_model):
        self._id = car_id
        self._race_model = race_model
        self._turbo = None
        self._pending_turbo = False
        self.state = TurboAI.ACCEPT_TURBO

    def append_new_turbo(self, json_boost):
        if self.state == self.ACCEPT_TURBO:
            self._turbo = (json_boost["turboDurationMilliseconds"],
                           json_boost["turboDurationTicks"],
                           json_boost["turboFactor"])

    #region Callbacks

    def on_turbo_start(self, json_data):
        car_id = json_data["name"], json_data["color"]
        if car_id != self._id:
            return
        current_tick = self._race_model.current_tick()
        logging.info("On TurboStart at {} requested at  {}".format(current_tick,
                                                                   self.old_gametick))

    def on_turbo_stop(self, json_data):
        car_id = json_data["name"], json_data["color"]
        if car_id != self._id:
            return
        current_tick = self._race_model.current_tick()
        logging.info("On TurboStop at {} requested at  {}".format(current_tick,
                                                                  self.old_gametick))

    def on_crash(self):
        self.state = TurboAI.DO_NOT_ACCEPT

    def on_spawn(self):
        self.state = TurboAI.ACCEPT_TURBO

    #endregion

    def update(self):
        if self._turbo == None:
            # We have no turbo
            return

        # Check if turbo is appropriate now
        current_segment = self._race_model.current_segment(self._id)
        if current_segment.type != TrackSegment.NORMAL:
            # Don't apply turbo on bends
            return

        turbo = self._turbo
        turbo_len = turbo[1]
        turbo_factor = turbo[2]

        index = self._race_model.current_piece_index(self._id)
        in_piece_distance = self._race_model.current_in_piece_distance(self._id)
        len_to_bend = current_segment.distance_to_next_segment(index,
                                                               in_piece_distance, 0)
        turbo_factor = turbo_factor * turbo_len * 2.5
        turbo_factor = min(turbo_factor, self._race_model.track.max_normal_segment_len()*0.8)

        if len_to_bend > turbo_factor:

            self.old_gametick = self._race_model.current_tick()
            logging.info("-----------> Applying Turbo, LenToBend {:.1f}, "
                         "Faktor: {:.1f}, Type: {:s}".format(len_to_bend,
                                                             turbo_factor,
                                                             TrackSegment.SEGMENT_NAMES[current_segment.type]))
            logging.info("Segment: {}".format(str(current_segment)))
            logging.info("Tick: {}".format(self.old_gametick))
            self._pending_turbo = True
            self._turbo = None

    @property
    def pending_turbo(self):
        if self._pending_turbo:
            self._pending_turbo = False
            return TurboAI.turbo_msg

