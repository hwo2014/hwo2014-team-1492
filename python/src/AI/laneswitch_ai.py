
import logging
from src.racemodel.track_piece import TrackPiece
from src.racemodel.track_segment import TrackSegment



class LaneSwitchAI(object):

    TAKE_INNER_LANE_DELTA = 1
    SWITCH_PIECE = [TrackPiece.SWITCH, TrackPiece.BENDSWITCH]

    BEFORE_SEGMENT, IN_SEGMENT, NOT_IN_SEGMENT = range(3)

    LEFT = "Left"
    RIGHT = "Right"


    def __init__(self, car_id, race_model):
        self.car_id = car_id
        self.race_model = race_model
        self.track = race_model.track

        self._switch_waiting_list = []
        self._pending_ignore_list = []

        self._next_switch_piece_index = None
        self._current_piece_index = None
        self._old_piece_index = None
        self._switch = None

        # Rules for lane switching. The later the rule the more important
        # Binary flag indicates if this rule is enabled
        self.rules = [
            (self.rule_take_outer_lane,                                         True),
            (self.rule_take_inner_lane_if_there_are_switches_before_and_after,  True),
            (self.rule_take_outer_if_we_are_coming_out_of_a_bend,               True),
           # (self.rule_drive_outer_line_and_switch_if_a_switch_is_in_bend,      True),
            (self.rule_switch_if_there_are_enemies_in_front,                    True),
            (self.rule_do_not_switch_if_there_are_enemies_beside,               True),
            ]

    def fetch_common_information(self):
        self._current_piece_index = self.race_model.current_piece_index(self.car_id)
        self._current_piece = self.track.piece_for_index(self._current_piece_index)
        self._current_segment = self.race_model.current_segment(self.car_id)
        self._next_switch_piece_index = self.track.next_switch_index(self._current_segment,
                                                                     self._current_piece_index)
        self._next_switch_segment = self.track.segment_for_index(self._next_switch_piece_index)
        self._coming_bends = self.track.bends_until_next_switch(self._next_switch_piece_index, 180)
        # Initialize that we're doing nothing
        self._switch = None

    def update(self):
        # Grab common information for all rules
        self.fetch_common_information()

        if self._current_piece.type in LaneSwitchAI.SWITCH_PIECE:
            # Do nothing on a switch
            return

        # Apply all rules
        for r in self.rules:
            if r[1]:
                r[0]()

    #region rules

    def rule_do_not_switch_if_there_are_enemies_beside(self):
        if self._switch is None:
            return

        current_lane = self.race_model.current_end_lane_index(self.car_id)

        if self._switch == LaneSwitchAI.RIGHT:
            lane = self.track.right_lane_index(current_lane)
        else:
            lane = self.track.left_lane_index(current_lane)

        cars = self.race_model.cars_for_lane(lane)

        if len(cars) == 0:
            # No cars on beside track
            return

        distance_to_cars = []
        for c in cars:
            d = self.race_model.distance_between_cars(self.car_id, c)
            distance_to_cars.append(d)

        # Filter out all distances in front
        distance_to_cars = filter(lambda d: d <= 0, distance_to_cars)

        if len(distance_to_cars) == 0:
            # No beneath us
            return

        if max(distance_to_cars) > -10:
            # if there cars near us, no not switch
            logging.info("Do not switch because of enemy")
            self._switch = None

    def rule_switch_if_there_are_enemies_in_front(self):
        current_lane = self.race_model.current_end_lane_index(self.car_id)
        cars_for_current_lane = self.race_model.cars_for_lane(current_lane)
        # Remove ourselves from car list
        cars_for_current_lane = filter(lambda c_id: c_id != self.car_id, cars_for_current_lane)

        if len(cars_for_current_lane) == 0:
            # No cars on same lane
            return

        distance_to_cars = []

        for c in cars_for_current_lane:
            d = self.race_model.distance_between_cars(self.car_id, c)
            distance_to_cars.append(d)

        # Remove all cars which are behind me
        distance_to_cars = filter(lambda d: d > 0, distance_to_cars)

        if len(distance_to_cars) == 0:
            # No cars in front of us
            return

        min_distance = min(distance_to_cars)

        if min_distance > 30:
            # Enemies are far away, no not switch
            return

        right_lane = self.track.right_lane_index(current_lane)
        left_lane = self.track.left_lane_index(current_lane)
        lanes = []
        if right_lane != current_lane:
            lanes.append(right_lane)
        if left_lane != current_lane:
            lanes.append(current_lane)

        if right_lane == current_lane:
            # There is no right lane, switch to left
            self._switch = LaneSwitchAI.LEFT
            logging.info("Switch to left because of enemy")
            return
        elif left_lane == current_lane:
            # There is no left lane, switch to right
            self._switch = LaneSwitchAI.RIGHT
            logging.info("Switch to right because of enemy")
            return

        logging.info("Switch to right because we have both sides")
        # We have both directions to switch
        self._switch = LaneSwitchAI.RIGHT


    def rule_drive_outer_line_and_switch_if_a_switch_is_in_bend(self):
        # Determine switch after next switch
        next_next_switch_piece_index = self.track.next_switch_index(self._next_switch_segment,
                                                                    self._next_switch_piece_index)
        next_next_switch_segment = self.track.segment_for_index(next_next_switch_piece_index)
        next_next_switch_piece = self.track.piece_for_index(next_next_switch_piece_index)

        if next_next_switch_piece.type != TrackPiece.BENDSWITCH:
            return

        # Determine outer lane for this segment
        self.take_outer_lane(next_next_switch_piece.angle)

    def rule_take_outer_if_we_are_coming_out_of_a_bend(self):
        # If we're in a piece, and there is a bend within DELTA
        # Take outer one
        if self._current_segment.type == TrackSegment.NORMAL:
            return

        current_indices = self._current_segment.available_indices()
        current_end_piece_index = current_indices[-1]

        if len(self._pending_ignore_list):
            if self._current_piece_index <= self._pending_ignore_list[0][0]:
                #if we didn't reached pending switch, ignore further switches
                return

        # Determine what's before
        piece_delta = self.track.piece_delta(current_end_piece_index,
                                             self._next_switch_piece_index)
        if abs(piece_delta) <= LaneSwitchAI.TAKE_INNER_LANE_DELTA:
            self.take_outer_lane(self._current_segment.angle)

    def rule_take_inner_lane_if_there_are_switches_before_and_after(self):

        if len(self._coming_bends) != 1:
            return

        # We already determined we're the first switch segment is. Determine
        # we next after this occurs
        next_next_switch_piece_index = self.track.next_switch_index(self._next_switch_segment,
                                                                    self._next_switch_piece_index)

        next_bend = self._coming_bends[0]
        bend_indices = next_bend.available_indices()
        bend_start_index = bend_indices[0]
        bend_end_index = bend_indices[-1]

        delta_index_start = bend_start_index - self._next_switch_piece_index
        delta_index_end = next_next_switch_piece_index - bend_end_index

        in_delta_start = delta_index_start <= LaneSwitchAI.TAKE_INNER_LANE_DELTA
        end_delta_start = delta_index_end <= LaneSwitchAI.TAKE_INNER_LANE_DELTA

        if in_delta_start and end_delta_start:
            # We're ready to switch
            self.take_inner_lane(next_bend.angle)


    def rule_take_outer_lane(self):
        if len(self._coming_bends) == 0:
            # No bends until next switch
            return

        max_angle = 0
        segment_of_interest = None

        for s in self._coming_bends:
            if abs(s.angle) > abs(max_angle):
                max_angle = s.angle
                segment_of_interest = s

        self.take_outer_lane(segment_of_interest.angle)

    #endregion

    #region predictor

    def predict_average_radius(self, segment):
        current_lane_index = self.race_model.current_start_lane_index(self.car_id)

        for switch in self._switch_waiting_list + self._pending_ignore_list:
            switch_index = switch[0]
            switch_direction = switch[1]
            switch_stat = self.segment_before_switch(segment, switch_index)

            if switch_stat == LaneSwitchAI.BEFORE_SEGMENT:
                if switch_direction == LaneSwitchAI.LEFT:
                    new_index = self.track.left_lane_index(current_lane_index)
                elif switch_direction == LaneSwitchAI.RIGHT:
                    new_index = self.track.right_lane_index(current_lane_index)
                else:
                    new_index = switch_direction

                # Get new lane offset
                lane_offset = self.track.lane_for_index(new_index)
                new_radius = segment.average_radius(lane_offset)
                return new_radius, lane_offset

        end_lane_index = self.race_model.current_end_lane_index(self.car_id)
        lane_offset = self.track.lane_for_index(end_lane_index)
        new_radius = segment.average_radius(lane_offset)
        return new_radius, lane_offset

    #endregion

    #region helpers

    def segment_before_switch(self, segment, switch_index):
        segment_indices = segment.available_indices()
        if switch_index < min(segment_indices):
            return LaneSwitchAI.BEFORE_SEGMENT
        elif switch_index in segment_indices:
            return LaneSwitchAI.IN_SEGMENT
        else:
            return LaneSwitchAI.NOT_IN_SEGMENT

    def replace_previous_switch(self, index, direction):
        for e in self._switch_waiting_list:
            if e[0] == index:
                e[1] = direction
                break
        else:
            if not self._index_in_list(index, self._pending_ignore_list):
                self._switch_waiting_list.append([index, direction])

    def take_outer_lane(self, angle):
        if angle > 0:
            # it's a right bend
            direction =  LaneSwitchAI.LEFT
        else:
            # it's a left bend
            direction = LaneSwitchAI.RIGHT

        self.replace_previous_switch(self._next_switch_piece_index,
                                     direction)

    def take_inner_lane(self, angle):
        if angle < 0:
            # it's a right bend
            direction = LaneSwitchAI.LEFT
        else:
            # it's a left bend
            direction = LaneSwitchAI.RIGHT

        self.replace_previous_switch(self._next_switch_piece_index,
                                     direction)

    #endregion

    #region properties

    def _index_in_list(self, index, l):
        for e in l:
            if e[0] == index:
                return True
        return False


    @property
    def pending_switch(self):
        switch_direction = None

        if self._old_piece_index != self._current_piece_index:
            if len(self._switch_waiting_list) != 0:
                switch_index, direction = self._switch_waiting_list[0]
                next_piece_index = self.track.next_piece_index(self._current_piece_index)

                if next_piece_index == switch_index:
                    if not self._index_in_list(switch_index, self._pending_ignore_list):
                        # Next piece is our switching target, do it baby
                        switch_direction = direction
                        if direction == LaneSwitchAI.LEFT:
                            lid = self.track.left_lane_index(self.race_model.current_start_lane_index(self.car_id))
                        else:
                            lid = self.track.right_lane_index(self.race_model.current_start_lane_index(self.car_id))

                        self._pending_ignore_list.append([switch_index, lid])
                    self._switch_waiting_list.pop(0)

            for switch_idx, lid in list(self._pending_ignore_list):
                if self._current_piece_index == self.track.next_piece_index(switch_idx):
                    self._pending_ignore_list.remove([switch_idx, lid])

        self._old_piece_index = self._current_piece_index
        return switch_direction

        #endregion
