from __future__ import division
import logging
from src.AI.turbo_ai import TurboAI

from src.racedata.racedatabase import RaceDataBase
from src.racedata.race_data_record import RaceDataRecord
from src.AI.break_ai import BreakAI
from src.AI.laneswitch_ai import LaneSwitchAI
from src.racemodel.bend_physics import BendPhysicsEstimator
from src.racemodel.break_process import BreakProcess
from src.racemodel.break_process_model import BreakProcessModel
from src.racemodel.race_model import RaceModel
from src.racemodel.track_piece import TrackPiece
from src.racemodel.track_segment import TrackSegment
from src.AI.angle_ai import AngleControl


class AIController(object):

    # This is a normal trackpiece
    NORMAL_PIECE = [TrackPiece.NORMAL, TrackPiece.SWITCH]

    def __init__(self, json_car):
        self.race_model = None
        self.race_data = None
        self.break_ai = None
        self._throttle = 0.5
        self.id = json_car["name"], json_car["color"]
        self.control = AngleControl(40.0)
        self.running = True
        self.old_segment = None
        self._fully_started_breaking = False
        self._last_break_process = None
        self.break_process_model = None
        self.race_cnt = 0
        self.angle_in_normal = 0
        self.crash_on_normal = False
        self.race_data = None


    def set_race(self, json_race):
        logging.info("Set Race, create all AIs")
        self.running = True
        self.race_model = RaceModel(json_race, self.race_cnt)
        self.race_cnt += 1
        if self.race_data is None:
            self.race_data = RaceDataBase(self.race_model)
            self.race_data.create_schema()
            self.race_data.put_track(self.race_model.track)
        else:
            self.race_data.race_model = self.race_model

        self.break_process_model = BreakProcessModel(self.race_model,
                                                     self.race_data)
        self.break_ai = BreakAI(self.id, self.race_model,
                               self.race_data, self.break_process_model)
        self.lane_switcher = LaneSwitchAI(self.id, self.race_model)
        self.race_data.lane_switcher = self.lane_switcher
        self.turbo_ai = TurboAI(self.id, self.race_model)
        self.bend_physics_estimator = BendPhysicsEstimator(self.id,
                                                           self.race_model,
                                                           self.race_data)


    def current_track_piece(self):
        index = self.race_model.current_track_piece_index(self.id)
        current_track_piece = self.race_model.track.piece_for_index(index)
        return current_track_piece

    def update_throttle_for_normal_trackpiece(self):
        # Grab information
        index = self.race_model.current_track_piece_index(self.id)
        lane_index = self.race_model.current_start_lane_index(self.id)
        lane_offset = self.race_model.track.left_lane_index(lane_index)
        in_piece_distance = self.race_model.current_in_piece_distance(self.id)

        # Determine how long it is to next bend
        distance_to_bend = self.race_model.track.distance_to_next_segment(index,
                                                                          lane_index,
                                                                          in_piece_distance)
        next_segment = self.race_model.next_segment(self.id)
        next_segment_angle = next_segment.angle

        lapInfo = self.race_model.lap(self.id)
        new_throttle, self._v_goal = self.break_ai.break_estimation(distance_to_bend,
                                                                    lapInfo is None,
                                                                    lane_offset)
        is_breaking = new_throttle < 1.0
        full_breaking = new_throttle == 0.0

        # What can happen?
        # 1) we're driving normally and start breaking
        #       1) new throttle == 0
        #       2) new 0 < throttle < 1
        #
        # 2) we already started breaking
        #       --> Throttle = 0
        #
        # 3) no time to breaking
        #   --> throttle = 1.0

        if not is_breaking:
            self._throttle = 1.0
            return False
        else:
            # Ignore bends which are less than 30 degree
            if abs(next_segment_angle) < 30:
                # Ignore little bends to break
                # TODO: maybe also check on radius?
                logging.info("easy to get through this bend --> fullspeed")
                logging.info(str(next_segment))
                self._throttle = 1.0
                return False

            # Apply new throttle
            self._throttle = new_throttle
            logging.debug("Breaking, Throttle {:.2f}".format(self._throttle))

            # We're breaking
            if full_breaking:
                if not self._fully_started_breaking:
                    self._fully_started_breaking = True
                    # Create a break process to store information about it
                    tick = self.race_model.current_tick()
                    self._last_break_process = BreakProcess(self.id, self.race_model,
                                                            distance_to_bend, tick)
            return True


    def update_throttle_for_bend_trackpiece(self):
        # Grab information
        index = self.race_model.current_track_piece_index(self.id)
        lane_index = self.race_model.current_start_lane_index(self.id)
        in_piece_distance = self.race_model.current_in_piece_distance(self.id)

        # Determine how long it is to next segment.
        # This can either be a normal segment or a bend in other direction
        distance_to_next_segment = self.race_model.track.distance_to_next_segment(index,
                                                                                  lane_index,
                                                                                  in_piece_distance)
        current_angle = self.race_model.angle(self.id)[0]
        v = self.race_model.velocity(self.id)[0]
        logging.info("Current velocity: {:.3f}".format(v))

        if distance_to_next_segment < 150:
            next_segment = self.race_model.next_segment(self.id)
            if next_segment.type != TrackSegment.NORMAL:
                # It's a bend in other direction
                logging.info("Bend: Angle={0} Before next bend {1}".format(current_angle,
                                                                           distance_to_next_segment))
                self.control.desired_angle = 45
            else:
                logging.info("Bend: Angle={0} Before normal {1}".format(current_angle,
                                                                        distance_to_next_segment))
                self.control.desired_angle = 55
        else:
            logging.info("Bend: Angle={0}".format(current_angle))
            self.control.desired_angle = 50

        # apply the correct sign for desired angle
        current_segment = self.race_model.current_segment(self.id)
        piece = current_segment.piece_for_index(index)
        if piece.angle < 0:
            self.control.desired_angle *= -1
            self.control.KpS = -1
        else:
            self.control.KpS = 1
        self._throttle = self.control.calc_throttle(current_angle)
        self.break_in_bend()


    def break_in_bend(self):
        # Grab information
        next_segment = self.race_model.next_segment(self.id)
        if next_segment.type == TrackSegment.NORMAL:
            return

        index = self.race_model.current_track_piece_index(self.id)
        lane_index = self.race_model.current_start_lane_index(self.id)
        lane_offset = self.race_model.track.left_lane_index(lane_index)
        in_piece_distance = self.race_model.current_in_piece_distance(self.id)

        # Determine how long it is to next bend
        distance_to_segment = self.race_model.track.distance_to_next_segment(index,
                                                                          lane_index,
                                                                          in_piece_distance)
        lapInfo = self.race_model.lap(self.id)
        new_throttle, self._v_goal = self.break_ai.break_estimation(distance_to_segment,
                                                                    lapInfo is None,
                                                                    lane_offset)

        if new_throttle < self._throttle:
            logging.info("We are breaking in a bend, v_curr: {:.2f}, v_goal {:.2f}".format(
                self.race_model.velocity(self.id)[0], self._v_goal))
            self._throttle = new_throttle

    def update(self, json_positions, game_tick):
        tick = self.race_model.update_positions(json_positions)
        current_track_piece = self.current_track_piece()
        self.event_monitoring(tick)

        if self.running:
            if current_track_piece.type in AIController.NORMAL_PIECE:
                self._fully_started_breaking = self.update_throttle_for_normal_trackpiece()
                self.control.reset()
            else:
                self.update_throttle_for_bend_trackpiece()
                self._fully_started_breaking = False
        else:
            self._throttle = 0
            self._fully_started_breaking = False
            self.control.reset()

        self.race_model.update_throttle(self.id, tick, self._throttle)
        self.bend_physics_estimator.update()
        self.lane_switcher.update()
        self.turbo_ai.update()

    def event_monitoring(self, tick):
        # Determine events such as input of bend, output of bend, crash
        new_segment = self.race_model.current_segment(self.id)
        if self.old_segment and self.old_segment != new_segment:
            # Segment has changed, determine what has changed
            if self.old_segment.type == TrackSegment.NORMAL:
                logging.info("We entered a bend from a normal track segment")
                # Save old data of normal track to DB
                self.race_data.put_to_normal_segment_table(self.old_segment.id,
                                                           self.angle_in_normal,
                                                           self.crash_on_normal)

                # We entered a bend from a normal track segment
                self.bend_physics_estimator.change_state(BendPhysicsEstimator.ARMED)
                v = self.race_model.velocity(self.id)[0]
                angle = self.race_model.angle(self.id)[0]
                omega = self.race_model.omega(self.id)[0]
                lane_index = self.race_model.current_start_lane_index(self.id)
                lane_offset = self.race_model.track.lane_for_index(lane_index)

                if self._last_break_process:
                    # We had a breaking process
                    breakpoint = self._last_break_process.start_breakpoint
                else:
                    # No breaking process
                    breakpoint = 0

                self.race_record = RaceDataRecord(tick, new_segment.id, self._v_goal,
                                                  v, angle, omega, breakpoint, lane_offset)

                if self._last_break_process:
                    # Set acceleration if we had a breaking process
                    logging.info("Starting with breaking----- {:.2f}".format(v))
                    self._last_break_process.end_break_point(tick)
                    self.race_record.acceleration = self._last_break_process.estimate_acceleration()
                    self.race_record.v_break_start = self._last_break_process.v_break_start
                    # Reset breaking process
                    self.break_process_model.append_break_process(self._last_break_process)
                    self._last_break_process = None

            elif new_segment.type == TrackSegment.NORMAL:
                # We left a bend to a normal track
                logging.info("We left a bend to a normal track")
                # Save omega in
                self.angle_in_normal = self.race_model.angle(self.id)[0]
                self.crash_on_normal = False

                self.bend_physics_estimator.change_state(BendPhysicsEstimator.NOT_ARMED)
                if hasattr(self, "race_record"):
                    # check if we have a race record because we might stared on a bend
                    # Grab information from last bend like maximum angle
                    max_angle = self.race_model.max_angle(self.id,
                                                          self.race_record.old_index,
                                                          tick-1)
                    max_omega = self.race_model.max_omega(self.id,
                                                          self.race_record.old_index,
                                                          tick-1)
                    self.race_record.max_angle = max_angle
                    self.race_record.omega_max = max_omega
                    # Save record to database
                    self.race_data.put_bend_record(self.race_model.track.id,
                                                   self.race_record)
            else:
                logging.info("We left a bend to drive into a new bend")
                if hasattr(self, "race_record"):
                    # check if we have a race record because we might stared on a bend
                    # We left a bend to drive into a new bend
                    max_angle = self.race_model.max_angle(self.id,
                                                          self.race_record.old_index,
                                                          tick-1)
                    max_omega = self.race_model.max_omega(self.id,
                                                          self.race_record.old_index,
                                                          tick-1)
                    self.race_record.max_angle = max_angle
                    self.race_record.omega_max = max_omega
                    # Save record to database
                    self.race_data.put_bend_record(self.race_model.track.id,
                                                   self.race_record)

                # We entered a bend from a bend track segment
                v = self.race_model.velocity(self.id)[0]
                angle = self.race_model.angle(self.id)[0]
                omega = self.race_model.omega(self.id)[0]
                lane_index = self.race_model.current_start_lane_index(self.id)
                lane_offset = self.race_model.track.lane_for_index(lane_index)

                self.race_record = RaceDataRecord(tick, new_segment.id, self._v_goal,
                                                  v, angle, omega, 0, lane_offset)
        # Save old segment in order to detect changes
        self.old_segment = new_segment

    def crashed(self, json_crash):
        car_id = json_crash["name"], json_crash["color"]
        if car_id != self.id:
            return

        s = self.race_model.current_segment(self.id)
        if s.type == TrackSegment.NORMAL:
            self.crash_on_normal = True

        logging.info("We crashed on segment {:d}, angle {:.2f}".format(s.id, s.angle))

        self.running = False
        self.race_record.crash = True
        self.turbo_ai.on_crash()

    def spawned(self, json_finish):
        car_id = json_finish["name"], json_finish["color"]
        if car_id != self.id:
            return
        self.control.reset()
        self.running = True
        self.turbo_ai.on_spawn()

    def on_finish(self):
        if self.break_process_model:
            self.break_process_model.export_coefficients()



    def on_game_end(self):
        # Reset game model
        if self.race_model:
            self.race_model.export_recordings()

    @property
    def throttle(self):
        return self._throttle


