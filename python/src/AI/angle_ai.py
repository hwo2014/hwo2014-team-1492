from __future__ import division
from math import copysign
import time


class AngleControl(object):
    def __init__(self, desired_angle):
        self.desired_angle = desired_angle

        self.current_time = time.time()
        self.previous_time = self.current_time
        self.previous_error = 0

        self.Cp = 0
        self.Ci = 0
        self.Cd = 0

        self.Kp = 0.1
        self.KpS = 1
        self.Ki = 0
        self.Kd = 1.5

    def calc_throttle(self, current_angle):

        error = self.desired_angle - current_angle

        self.current_time = time.time()
        dt = 1#self.current_time - self.previous_time
        de = error - self.previous_error

        self.Cp = self.Kp * error * self.KpS
        self.Ci += error * copysign(1, current_angle) * dt

        self.Cd = 0
        if dt > 0:
            self.Cd = de/dt

        #self.previous_time = self.current_time
        self.previous_error = error

        val = self.Cp + (self.Ki * self.Ci) + (self.Kd * self.Cd * self.KpS)

        '''
        logging.info("Desired: {:.2f} Ang: {:.2f} Err: {:.2f} dE: {:.2f}  P: {:.2f} "
                     "D: {:.2f} Ci: {:.2f} Val: {:.2f}".format( self.desired_angle,
                                                                current_angle,
                                                                error,
                                                                de,
                                                                self.Cp,
                                                                self.Cd * self.Kd * self.KpS,
                                                                self.Ci,
                                                                val))
        '''
        if val < 0.0: val = 0.0
        if val > 1.0: val = 1.0

        return val

    def reset(self):
        self.previous_error = 0

        self.Cp = 0
        self.Ci = 0
        self.Cd = 0