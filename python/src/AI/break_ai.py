

import logging
from src.racemodel.track import TrackSegment


class BreakAI(object):

    MAX_ANGLE = 55

    def __init__(self, id, race_model, race_data, break_process_model):
        self.id = id
        self.race_model = race_model
        self.race_data = race_data
        self.break_process_model = break_process_model

    def break_estimation(self, bend_distance, first_round, lane_offset=0):
        if self.race_model.race_type == "Race":
             # We already drove our first rounds when being in a race
            return self.break_distance_with_current_data(bend_distance, lane_offset)
        if first_round:
            return self.break_distance_for_first_round(bend_distance, lane_offset)
        else:
            return self.break_distance_with_current_data(bend_distance, lane_offset)

    def break_distance_for_first_round(self, distance_to_segment, lane_offset):
        next_segment = self.race_model.next_segment(self.id)
        segment_len = next_segment.length(lane_offset)
        next_next_segment = self.race_model.track.next_segment(next_segment)

        v_goal_next = self.race_data.v_slip(next_segment)
        throttle_next = self._estimate_throttle(distance_to_segment, v_goal_next)

        if next_next_segment.type == TrackSegment.NORMAL:
            throttle = throttle_next
            v_goal = v_goal_next
            logging.info("Next next segment is a normal piece do not count in")
        else:
            v_goal_next_next = self.race_data.v_slip(next_next_segment)
            throttle_next_next = self._estimate_throttle(distance_to_segment+segment_len, v_goal_next_next)

            logging.info("throttle_next {:.2f}, throttle next next {:.2f}".format(
            throttle_next, throttle_next_next
            ))

            if throttle_next_next < 1.0:
                throttle =  throttle_next_next
                v_goal = v_goal_next_next
                logging.info("We're breaking because to reach segment after!!")
            else:
                throttle = throttle_next
                v_goal = v_goal_next

        return throttle, v_goal

    def break_distance_with_current_data(self, distance_to_segment, lane_offset):
        next_segment = self.race_model.next_segment(self.id)
        segment_len = next_segment.length(lane_offset)
        next_next_segment = self.race_model.track.next_segment(next_segment)
        track_id = self.race_model.track.id

        last_segment_data = self.race_data.current_data_for_segment(track_id, next_segment)[-1]

        next_crashed = False
        if next_next_segment.type == TrackSegment.NORMAL:
            logging.info("Segment after bend is normal trace")
            data = self.race_data.get_normal_segment(next_next_segment.id)
            if len(data) > 0:
                last_data = data[-1]
                next_crashed = last_data[1]

        v_goal_next = self._calc_vgoal(last_segment_data, next_crashed)

        logging.info("v_goal: {:.2f}".format(v_goal_next))
        if self.race_model.race_type == "Race" and self.race_model.lap(self.id) == None:
            v_goal_next *= 0.95
            logging.info("Reducing v_goal to {:.2f}".format(v_goal_next))
        throttle_next = self._estimate_throttle(distance_to_segment, v_goal_next)

        if next_next_segment.type != TrackSegment.NORMAL:
            last_next_segment_data = self.race_data.current_data_for_segment(track_id, next_next_segment)[-1]
            v_goal_next_next = self._calc_vgoal(last_next_segment_data)
            if self.race_model.race_type == "Race" and self.race_model.lap(self.id) == None:
                v_goal_next_next *= 0.95
            throttle_next_next = self._estimate_throttle(distance_to_segment+segment_len, v_goal_next_next)

            if throttle_next_next < 1.0:
                throttle =  throttle_next_next
                v_goal = v_goal_next_next
                logging.info("We're breaking because to reach segment after!!")
            else:
                throttle = throttle_next
                v_goal = v_goal_next
        else:
            throttle = throttle_next
            v_goal = v_goal_next

        logging.info("Calculated v_goal: {:.2f}".format(v_goal))
        return throttle, v_goal

    #region private

    def _calc_vgoal(self, last_segment_data, next_crashed=False):
        angle = last_segment_data.angle_max
        v_old = last_segment_data.v_in

        e = BreakAI.MAX_ANGLE - abs(angle)

        if last_segment_data.crash:
            v_goal = last_segment_data.v_goal * 0.7
        elif next_crashed:
            v_goal = last_segment_data.v_goal * 0.85
        else:
            if self.race_model.race_type == "Qualifying":
                if e < 3:
                    MU = 1/80.
                elif e > 17:
                    MU = 1/30.
                else:
                    MU = 1/40.
            else:
                # Smaller learning rates
                if e > 10:
                    MU = 1/200.
                else:
                    MU = 0.
                logging.info("This is a race. Learning is disabled")

            v_goal = v_old + MU * e

        return v_goal

    def _estimate_throttle(self, distance_to_bend, v_goal):
        v_current = self.race_model.velocity(self.id)[0]
        v_end = self._reachable_velocity(v_current, distance_to_bend)
        v_end_next = self._reachable_velocity(v_current, distance_to_bend - v_current)

        if v_current > 10.0:
            v_end = v_end * 1.2

        if v_end >= v_goal and v_end_next >= v_goal:
            # Break it baby
            throttle = 0.0
        elif v_end < v_goal and v_end_next >= v_goal:
            # we're in between breaking
            throttle = (v_goal - v_end) / (v_end_next - v_end)
        else:
            # No need to break
            throttle = 1.0

        logging.info("Distance to bend: {:.2f}, V_goal {:.2f}, v_current: {:.2f}, "
                     "v_end {:.2f}".format(
            distance_to_bend, v_goal, v_current, v_end
        ))

        return throttle


    def _reachable_velocity(self, v_current, s):
        k = self.break_process_model.acceleration_coefficient
        v_end =  v_current + k * s
        return max(0, v_end)

    def _find_velocity_for_nearest_radius(self, target_angle, velocity_data):
        v_nearest = 0
        current_angle = -1
        for e in velocity_data:
            if current_angle == -1:
                current_angle = e[0]
                v_nearest = e[1]
                continue
            if abs(e[0] - target_angle) < abs(current_angle - target_angle):
                current_angle = e[0]
                v_nearest = e[1]
        return v_nearest

    #endregion

