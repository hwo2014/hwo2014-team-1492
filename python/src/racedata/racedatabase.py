
import os
import math
import logging
import sqlite3
from src.racemodel.track_segment import TrackSegment
from src.racedata.race_data_record import RaceDataRecord
from src.util.utils import abs_list


class RaceDataBase(object):
    DB_NAME = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "race_data.db")

    OLD_BEND_TABLE = "oldRecords"
    CURRENT_BEND_TABLE = "currentRecords"

    def __init__(self, race_model):
        self.race_model = race_model
        if os.path.isfile(RaceDataBase.DB_NAME):
            logging.info("Database already exists")
        else:
            logging.info("No database found")

        #self.conn = sqlite3.connect(RaceDataBase.DB_NAME)
        self.conn = sqlite3.connect(":memory:")
        self.cursor = self.conn.cursor()
        # Enable foreign keys
        self.cursor.execute("PRAGMA foreign_keys = ON")
        # self.DROP_ALL()
        self.cursor.execute("DROP TABLE IF EXISTS vSlipCurrent")

    def DROP_ALL(self):
        self.cursor.execute("DROP TABLE IF EXISTS normalSegmentRecords")
        self.cursor.execute("DROP TABLE IF EXISTS accelerationCoefficients")
        self.cursor.execute("DROP TABLE IF EXISTS goldenParams")
        self.cursor.execute("DROP TABLE IF EXISTS oldRecords")
        self.cursor.execute("DROP TABLE IF EXISTS currentRecords")
        self.cursor.execute("DROP TABLE IF EXISTS segments")
        self.cursor.execute("DROP TABLE IF EXISTS tracks")

    def dump_db(self):
        dump = "\n"
        for l in self.conn.iterdump():
            dump += l + "\n"
        logging.info(dump)

    def export(self):
        # Move all data from current run to persistent table
        self.cursor.execute("""
          INSERT INTO oldRecords
          SELECT * FROM currentRecords
        """)
        self.cursor.execute("DELETE FROM currentRecords")
        self.conn.commit()

        # Now we've saved current data we can update our golden parameterset
        self.calculate_golden_parameter_set()

    def close(self):
        # Dump al data
        # self.dump_db()

        # Close DB because we're finished
        self.conn.close()

    #region DB getters

    def current_data_for_segment(self, track_id, segment):
        segment_key = self._segment_key(segment.id, track_id)
        self.cursor.execute("""
          SELECT vGoal, vIn, angleIn, angleMax, omegaIn, omegaMax,
                 laneOffset, acceleration, vBreakStart, breakpoint, crash
                    FROM currentRecords
            WHERE currentRecords.trackId=(?) AND
                  currentRecords.segmentKey=(?)
        """, (track_id, segment_key))
        data = self.cursor.fetchall()

        records = []
        for row in data:
            r = RaceDataRecord()
            r.fill_data(segment.id, *row)
            records.append(r)
        return records

    def max_radius(self, track_id):
        self.cursor.execute("""
          SELECT MAX(maxRadius) maxRadius FROM segments WHERE trackId=(?)
        """, (track_id, ))
        return self.cursor.fetchone()[0]

    def acceleration_coefficients(self, track_id, op):
        assert op in ["==", "!="]
        self.cursor.execute("""
          SELECT meanCoefficient, medianCoefficient from accelerationCoefficients
            where trackId """ + op + """ (?)
        """, (track_id,))
        records = self.cursor.fetchall()
        # Return mean and median as list
        return(zip(*records))

    #endregion

    #region DB putters

    def put_track(self, track):
        """
        Puts the segments of a given track into the database
        """
        # Check if we already have this track
        self.cursor.execute("SELECT * FROM tracks WHERE name=(?)", (track.id, ))
        if self.cursor.fetchone():
            logging.info("Track {0} already in database".format(track.id))
            return

        self.cursor.execute("INSERT INTO tracks VALUES (?)", (track.id,))

        for s in track.segments:
            self.cursor.execute("""INSERT INTO segments(
              segmentId, trackId, type, angle, averageRadius)
              VALUES (?, ?, ?, ?, ?)
            """, (s.id, track.id, s.SEGMENT_NAMES[s.type], s.angle,
                  s.average_radius()))

        self.conn.commit()
        logging.info("Track {0} put to database".format(track.id))

    def put_bend_record(self, track_id, race_record):
        """
        Puts a new log from a bend into the database
        """
        self._put_to_bend_table(RaceDataBase.CURRENT_BEND_TABLE,
                                track_id, race_record)

    def put_acceleration_coefficient(self, track_id,
                                     mean_coefficient,
                                     median_coefficient):
        self.cursor.execute("""
            INSERT INTO accelerationCoefficients(
              trackId, meanCoefficient, medianCoefficient)
                VALUES (?,?,?)
        """, (track_id, mean_coefficient, median_coefficient))
        self.conn.commit()

    #endregion

    #region DB Creators

    def create_schema(self):
        """
        Creates a default schema for the database
        """
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS tracks(
          name  TEXT PRIMARY KEY NOT NULL)
        """)

        self.cursor.execute("""CREATE TABLE IF NOT EXISTS segments(
          id            INTEGER PRIMARY KEY AUTOINCREMENT,
          segmentId     INTEGER,
          trackId       TEXT NOT NULL,
          type          TEXT NOT NULL,
          angle         REAL,
          averageRadius REAL,

          FOREIGN KEY (trackId) REFERENCES tracks(name)
        )
        """)

        self.create_bend_table(RaceDataBase.CURRENT_BEND_TABLE)
        self.create_bend_table(RaceDataBase.OLD_BEND_TABLE)

        self.cursor.execute("""
          CREATE TABLE IF NOT EXISTS goldenParams(
              id            INTEGER PRIMARY KEY AUTOINCREMENT ,
              angle         REAL,
              averageRadius REAL,
              vGoal         REAL,
              bendType      TEXT)
        """)

        self.cursor.execute("""
          CREATE TABLE IF NOT EXISTS accelerationCoefficients(
            id                INTEGER PRIMARY KEY AUTOINCREMENT,
            trackId           TEXT,
            meanCoefficient   REAL,
            medianCoefficient REAL)
        """)

        self.cursor.execute("""
          CREATE TABLE IF NOT EXISTS vSlipCurrent(
            id          INTEGER PRIMARY KEY AUTOINCREMENT,
            trackId     TEXT NOT NULL,
            segmentKey  INTEGER,
            laneOffset  REAL,
            vSlip       REAL,
            aSlip       REAL,

            FOREIGN KEY (trackId) REFERENCES tracks(name),
            FOREIGN KEY (segmentKey) REFERENCES segments(id)
          )
        """)

        self.cursor.execute("""
          CREATE TABLE IF NOT EXISTS normalSegmentRecords(
            id      INTEGER PRIMARY KEY AUTOINCREMENT,
            trackId TEXT,
            segmentKey  INTEGER,
            angleIn     REAL,
            crash       BOOLEAN,

            FOREIGN KEY (trackId) REFERENCES tracks(name),
            FOREIGN KEY (segmentKey) REFERENCES segments(id)
          )
        """)

        logging.info("DB Schema created")


    def create_bend_table(self, name):
        self.cursor.execute("CREATE TABLE IF NOT EXISTS "
                            + name + """ (
          id            INTEGER PRIMARY KEY AUTOINCREMENT,
          trackId       TEXT,
          segmentKey    INTEGER,
          vGoal         REAL,
          vIn           REAL,
          angleIn       REAL,
          angleMax      REAL,
          omegaIn       REAL,
          omegaMax      REAL,
          laneOffset    REAL,
          acceleration  REAL,
          vBreakStart   REAL,
          breakpoint    REAL,
          crash         BOOLEAN,

          FOREIGN KEY (trackId) REFERENCES tracks(name),
          FOREIGN KEY (segmentKey) REFERENCES segments(id)
        )
        """)


    #endregion

    def _get_distinct_bend_classes(self):
        self.cursor.execute("""
            SELECT DISTINCT ABS(segments.angle),
              CASE
                WHEN segments.angle < 0
                  THEN segments.averageRadius + oldRecords.laneOffset
                ELSE segments.averageRadius - oldRecords.laneOffset
              END AS radius
            FROM oldRecords
              INNER JOIN segments ON oldRecords.segmentKey = segments.id
        """)
        classes = self.cursor.fetchall()
        return classes

    def _get_records(self, angle, effective_radius, crash):
        self.cursor.execute("""
            SELECT vIn, angleMax, omegaMax FROM (
              SELECT oldRecords.vIn, oldRecords.angleMax, oldRecords.omegaMax,
                  CASE
                    WHEN segments.angle < 0
                      THEN segments.averageRadius + oldRecords.laneOffset
                    ELSE segments.averageRadius - oldRecords.laneOffset
                  END AS radius
                FROM oldRecords
                  INNER JOIN segments ON oldRecords.segmentKey = segments.id
                  WHERE radius = (?) AND
                        ABS(segments.angle) = (?) AND
                        oldRecords.crash = (?)
                  )
            """, (effective_radius, angle,  int(crash)))

        records = self.cursor.fetchall()
        return records

    def _add_golden_parameter_class(self, angle, effective_radius, type, v_golden):
        self.cursor.execute("""INSERT INTO goldenParams (angle, averageRadius, vGoal, bendType)
                               VALUES (?, ?, ?, ?)""",
                            (angle, effective_radius, v_golden, type))
        self.conn.commit()

    def calculate_golden_parameter_set(self):
        logging.info("Calculating golden parameters")

        # clear golden parameters
        self.cursor.execute("DELETE FROM goldenParams")
        # determine all segments classes

        ### BENDS ###
        classes = self._get_distinct_bend_classes()
        for c in classes:
            angle = c[0]
            effective_radius = c[1]
            good_records = self._get_records(angle, effective_radius, False)
            crash_records = self._get_records(angle, effective_radius, True)
            if len(good_records) > 0:
                v_golden = self._calc_golden_v_goal(good_records, crash_records)
                self._add_golden_parameter_class(angle, effective_radius, "bend", v_golden)

        logging.info("Calculate golden parameters finished")

    def _calc_golden_v_goal(self, good_records, crash_records):
        MU_GOOD = 1/20.
        MU_BAD = 1/20.0
        ANGLE_MAX = 60.0
        ANGLE_IGNORE = 55.0

        good_angles = zip(*good_records)[1]
        min_good_angle = min(abs_list(good_angles))
        max_good_angle = max(abs_list(good_angles))
        delta_angle_max = max_good_angle - min_good_angle

        v_goal = 0
        for r in good_records:
            v_in = r[0]
            angle_max = r[1]
            omega_max = r[2]

            e = ANGLE_IGNORE- abs(angle_max)

            if e < 3:
                MU_pos = 1/80.
                MU_neg = 1/10.
            elif e > 17:
                MU_pos = 1/10.
                MU_neg = 1/10.
            else:
                MU_pos = 1/40.
                MU_neg = 1/20.

            if e > 0:
                MU = MU_pos
            else:
                MU = MU_neg

            v_goal += v_in + MU * e

        for r in crash_records:
            v_in = r[0]
            angle_max = r[1]
            omega_max = r[2]
            v_goal += v_in * 0.9

        v_goal /= (len(good_records) + len(crash_records))
        return v_goal

    def v_slip(self, segment):
        _, lane_offset = self.lane_switcher.predict_average_radius(segment)

        segment_key = self._segment_key(segment.id,
                                        self.race_model.track.id)
        self.cursor.execute("""
          SELECT vslip FROM vSlipCurrent WHERE segmentKey = (?) AND laneOffset = (?)
        """, (segment_key, lane_offset))

        v_slip = self.cursor.fetchall()

        if len(v_slip) == 1:
            # We found value in database
            return v_slip[0][0]

        # Else calculate default
        # TODO: If this is a proper choice
        a_slip = 0.3
        r = segment.average_radius(lane_offset)
        v_slip = self._calc_v_slip(a_slip, r)

        logging.info("We do not have an v_slip estimation, "
                     "Calculate v_slip = {:.2f} with a_slip {:.3f}".format(
            v_slip, a_slip
        ))
        return v_slip

    def calculate_v_goal(self, segment):
        angle = abs(segment.angle)
        average_radius, _ = self.lane_switcher.predict_average_radius(segment)

        self.cursor.execute("""
          SELECT vGoal FROM goldenParams
             WHERE angle = (?) AND averageRadius = (?)""",
            (angle, average_radius))

        v = self.cursor.fetchall()
        assert len(v) <= 1

        if len(v) == 1:
            return v[0][0]
        else:
            # we didn't find any matching segment. Therefore calculate a
            # v_goal from similar segments
            return self.calc_goal_from_similar_segments(segment)

    def calc_goal_from_similar_segments(self, segment):
        angle = abs(segment.angle)

        # Find all segments with same angle
        self.cursor.execute("""
          SELECT vGoal, averageRadius FROM goldenParams WHERE angle >= (?)
        """, (angle, ))

        records = self.cursor.fetchall()

        if len(records) == 0:
            # Fallback if there are no similar bends
            return 4

        estimated_v_goal = 0
        average_radius, _ = self.lane_switcher.predict_average_radius(segment)

        for r in records:
            v_goal = r[0]
            radius = r[1]
            # average/radius = 1 ---> vgoal * sqrt(2)^0
            # average/radius = 2 ---> vgoal * sqrt(2)^1
            # average/radius = 4 ---> vgoal * sqrt(2)^2
            # average/radius = 8 ---> voagl * sqrt(2)^3
            radius_ratio = radius / average_radius
            estimated_v_goal += v_goal * math.sqrt(2)**math.log(radius_ratio, 2)

        estimated_v_goal /= len(records)

        logging.info("Estimated v_goal for radius {:.1f}, angle {:.1f}: {:.3f}".format(
            average_radius, angle, estimated_v_goal
        ))

        return estimated_v_goal

    def calculate_v_slip(self, a_slip):
        logging.info("Predicting v_slip")
        for s in self.race_model.track.segments:
            if s.type == TrackSegment.NORMAL:
                continue
            logging.info("Predicting for segment {:d}".format(s.id))
            segment_key = self._segment_key(s.id, self.race_model.track.id)
            for l in self.race_model.track.lanes:
                r = s.average_radius(l)
                v_slip = self._calc_v_slip(a_slip, r)
                self._put_to_v_slip_table("vSlipCurrent", segment_key, a_slip, v_slip, l)
        self.conn.commit()

    #region helpers

    def _calc_v_slip(self, a_slip, r):
        v_slip = math.sqrt(a_slip * r)
        return v_slip

    def _segment_key(self, segment_id, track_id):
        self.cursor.execute("""SELECT id FROM segments
                               WHERE trackId=(?) AND segmentId=(?)
        """, (track_id, segment_id))
        segment_ids = self.cursor.fetchall()

        assert len(segment_ids) == 1
        return segment_ids[0][0]

    def get_normal_segment(self, segment_id):
        track_id = self.race_model.track.id
        segment_key = self._segment_key(segment_id, track_id)

        self.cursor.execute("""
          SELECT angleIn, crash FROM normalSegmentRecords where trackId = (?) AND segmentKey = (?)
        """, (track_id, segment_key))
        r = self.cursor.fetchall()
        return r

    def put_to_normal_segment_table(self, segment_id, angle_in, crash):
        track_id = self.race_model.track.id
        segment_key = self._segment_key(segment_id, track_id)

        self.cursor.execute("""INSERT into normalSegmentRecords
          (trackId, segmentKey, angleIn, crash)
          VALUES (?,?,?,?)
        """, (track_id, segment_key, angle_in, crash))
        self.conn.commit()

    def _put_to_bend_table(self, table, track_id, race_record):
        segment_key = self._segment_key(race_record.segment_id, track_id)

        self.cursor.execute("INSERT INTO " + table + """
          (trackId, segmentKey, vGoal, vIn, angleIn, angleMax,
          omegaIn, omegaMax, laneOffset, acceleration, vBreakStart,
          breakpoint, crash)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
        """, (track_id,
              segment_key,
              race_record.v_goal,
              race_record.v_in,
              race_record.angle_in,
              race_record.max_angle,
              race_record.omega_in,
              race_record.omega_max,
              race_record.lane_offset,
              race_record.acceleration,
              race_record.v_break_start,
              race_record.breakpoint,
              race_record.crash))
        self.conn.commit()

    def _put_to_v_slip_table(self, table, segment_key, a_slip, v_slip, lane_offset):
        self.cursor.execute("""
          INSERT INTO """ + table + """
            (trackId, segmentKey, laneOffset, aSlip, vSlip)
            VALUES (?, ?, ?, ?, ?)
        """, (self.race_model.track.id,
              segment_key, lane_offset, a_slip, v_slip))


    #endregion
