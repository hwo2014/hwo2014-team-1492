
import logging


class RaceDataRecord(object):
    def __init__(self, index=0, segment_id=0, v_goal=0, v_in=0,
                 angle_in=0, omega_in=0, breakpoint=0, lane_offset=-1):

        logging.info("Creating race record for segment id {}".format(
            segment_id
        ))
        self.segment_id = segment_id
        self.old_index = index
        self.v_goal = v_goal
        self.v_in = v_in
        self.angle_in = angle_in
        self.angle_max = -1
        self.omega_in = omega_in
        self.omega_max = 0
        self.breakpoint = breakpoint
        self.acceleration = 0
        self.v_break_start = 0
        self.crash = False
        self.lane_offset = lane_offset

    def __str__(self):
        return "\nSegmentID: {:d}\n" \
               "v_goal:    {:.2f}\n" \
               "v_in:      {:.2f}\n" \
               "Angle In:  {:.2f}\n" \
               "Angle Max: {:.2f}\n" \
               "OmegaIn:   {:.2f}\n" \
               "OmegaMax:  {:.2f}\n" \
               "Accel:     {:.2f}\n" \
               "Breakpoint {:.2f}\n" \
               "Crash:     {:d}".format(self.segment_id,
                                        self.v_goal,
                                        self.v_in,
                                        self.angle_in,
                                        self.angle_max,
                                        self.omega_in,
                                        self.omega_max,
                                        self.acceleration,
                                        self.breakpoint,
                                        self.crash)

    def fill_data(self, segment_id, v_goal, v_in, angle_in, angle_max,
                  omega_in, omega_max, lane_offset, acceleration,
                  v_break_start, breakpoint, crash):
        self.segment_id = segment_id
        self.v_goal = v_goal
        self.v_in = v_in
        self.angle_in = angle_in
        self.angle_max = angle_max
        self.omega_in = omega_in
        self.omega_max = omega_max
        self.lane_offset = lane_offset
        self.acceleration = acceleration
        self.v_break_start = v_break_start
        self.breakpoint = breakpoint
        self.crash = bool(crash)
