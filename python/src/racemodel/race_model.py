import logging
import os
import csv
from datetime import datetime
from src.racemodel.lap_information import LapInformation

from src.racemodel.track import Track
from src.racemodel.car import Cars
from src.racemodel.car_position import CarPosition

from src.util.signal_processing import diff
from src.util.utils import seq_in_seq


class RaceModel(object):

        def __init__(self, json_race, race_type=None):
            self.track = Track(json_race["track"])
            self.cars = Cars(json_race["cars"])
            session = json_race["raceSession"]

            if race_type == 0:
                logging.info("It's a qualifying baby or quick race")
                self.race_type = "Qualifying"
            else:
                logging.info("Second race model, it's the actual race")
                self.race_type = "Race"

            try:
                self.laps = int(session["laps"])
                self.max_lap_time = int(session["maxLapTimeMs"])
                self.quick_race = session["quickRace"]
                self.race_duration = -1
                logging.info("Started a race")
            except KeyError:
                # It's a qualifying
                self.race_duration = session["durationMs"]
                self.laps = -1
                self.max_lap_time = -1
                self.quick_race = False
                logging.info("Started a qualifying or quick race")

            self.car_positions = {}
            self.car_laps = {}

        def __str__(self):
            return "Track: {0} Cars: {1} Laps: {2} " \
                   "MaxLapTime: {3} QuickRace: {4}".format(self.track,
                                                           self.cars,
                                                           self.laps,
                                                           self.max_lap_time,
                                                           self.quick_race)

        #region Update current State

        def update_positions(self, json_positions):
            for position in json_positions:
                c = CarPosition(position)
                self.car_positions.setdefault(c.id, [])
                self.car_positions[c.id].append(c)

            return len(next(self.car_positions.itervalues())) - 1

        def update_throttle(self, car_id, game_tick, throttle):
            self.car_positions[car_id][game_tick].throttle = throttle

        def update_lap(self, json_lap):
            id = json_lap["car"]["name"], json_lap["car"]["color"]
            self.car_laps.setdefault(id, LapInformation()).add_lap(json_lap)

        #endregion

        #region Current State getter

        def car_ids(self):
            return list(self.car_positions.iterkeys())

        def cars_for_lane(self, lane_index):
            """
            Returns all cars which currently are on this lane
            """
            cars = []
            current_tick = self.current_tick()
            for car_id, data in self.car_positions.iteritems():
                if self.car_positions[car_id][current_tick].piece_position.end_lane_index == lane_index:
                    cars.append(car_id)
            return cars

        def lap(self, car_id):
            try:
                return self.car_laps[car_id]
            except KeyError:
                return None

        def piece_index(self, car_id, count=1):
            """
            :param car_id
            :param count    if count is -1, all available distance values
                            are returned
            """
            game_index = len(self.car_positions[car_id])-1
            if count == -1:
                count = len(self.car_positions[car_id])

            piece_indices = []
            for i in xrange(count):
                piece_indices.append(self.car_positions[car_id][game_index-i].piece_position.piece_index)
            return piece_indices

        # getters for physical data

        def angle(self, car_id, count=1):
            """
            :param car_id
            :param count    if count is -1, all available angle values
                            are returned
            """
            game_index = len(self.car_positions[car_id])-1
            if count == -1: count = len(self.car_positions[car_id])

            angles = []
            for i in xrange(count):
                angles.append(self.car_positions[car_id][game_index-i].angle)
            return angles

        def max_angle(self, car_id, index_begin, index_end):
            """
            Returns the maximum angle for given car and given index range.
            The maximum is meant in an absolute way
            """
            amount = index_end - index_begin + 1
            angles = self.angle(car_id, amount)

            if len(angles) == 0:
                # We do not have any angles for some reason
                return 0

            max_angle = max(angles)
            min_angle = min(angles)
            if abs(max_angle) > abs(min_angle):
                return max_angle
            else:
                return min_angle

        def max_omega(self, car_id, index_begin, index_end):
            """
            Returns the maximum omega for given car and given index range.
            The maximum is meant in an absolute way
            """
            amount = index_end - index_begin + 1
            omegas = self.omega(car_id, amount)

            if len(omegas) == 0:
                # We do not have any omegas for some reason
                logging.info("We don't have any omegas for amount {}".format(amount))
                return 0

            omegas = list(reversed(omegas))

            crashed_index = seq_in_seq([0, 0], omegas)
            if crashed_index != -1:
                # We might crashed, ignore that
                omegas = omegas[0:crashed_index-1]

            if len(omegas) == 0:
                # We do not have any omegas for some reason
                logging.info("We don't have any omegas for amount {}".format(amount))
                return 0

            max_omega = max(omegas)
            min_omega = min(omegas)

            if abs(max_omega) > abs(min_omega):
                extremum_omega = max_omega
            else:
                extremum_omega = min_omega

            return extremum_omega

        def omega(self, car_id, count=1):
            """
            :param car_id
            :param count    if count is -1, all available angular velocity values
                            are returned
            """
            angles = self.angle(car_id, count+1 if count != -1 else count)
            if count == -1:
                count = len(self.car_positions[car_id])
                angles.append(0)

            return diff(angles)

        def distance(self, car_id, count=1):
            """
            :param car_id
            :param count    if count is -1, all available distance values
                            are returned
            """
            game_index = len(self.car_positions[car_id])-1
            if count == -1:
                count = len(self.car_positions[car_id])

            distances = []
            for i in xrange(count):
                distances.append(self.car_positions[car_id][game_index-i].piece_position.in_piece_distance)
            return distances

        def velocity(self, car_id, count=1):
            """
            :param car_id
            :param count    if count is -1, all available angular velocity values
                            are returned
            """
            # v = Delta S
            v = []

            if count == -1:
                count = len(self.car_positions[car_id])

            # Determine count+1 positions from the past
            game_index = len(self.car_positions[car_id])-1

            p1 = self.car_positions[car_id][game_index].piece_position.piece_index
            s1 = self.car_positions[car_id][game_index].piece_position.in_piece_distance
            sli1 = self.car_positions[car_id][game_index].piece_position.start_lane_index
            eli1 = self.car_positions[car_id][game_index].piece_position.end_lane_index

            for i in xrange(1, count+1):
                p2 = self.car_positions[car_id][game_index-i].piece_position.piece_index
                s2 = self.car_positions[car_id][game_index-i].piece_position.in_piece_distance
                sli2 = self.car_positions[car_id][game_index-i].piece_position.start_lane_index
                eli2 = self.car_positions[car_id][game_index-i].piece_position.end_lane_index

                if p1 == p2:
                    # Same segments
                    delta_dst = s1 - s2
                else:
                    # different segments
                    piece2 = self.track.piece_for_index(p2)

                    if sli1 != eli1 and sli2 == eli2:
                        # We drove into a switch
                        p2_len = piece2.calc_length(self.track.lane_for_index(sli2))
                        delta_dst = p2_len - s2 + s1
                    elif sli2 != eli2:
                        # we came out of a switch, or came out from a switch
                        # to another one
                        l1 = self.track.lane_for_index(sli2)
                        l2 = self.track.lane_for_index(eli2)
                        p2_len = piece2.switch_length(abs(l1) + abs(l2))
                        delta_dst = p2_len - s2 + s1
                    else:
                        # We do not drive on a switch
                        lane_offset = self.track.lane_for_index(eli2)
                        p2_len = piece2.calc_length(lane_offset)
                        delta_dst = p2_len - s2 + s1

                p1 = p2
                s1 = s2
                sli1 = sli2
                eli1 = eli2
                v.append(delta_dst)
            return v

        def throttle(self, car_id, count=1):
            """
            :param car_id
            :param count    if count is -1, all available throttle values
                            are returned
            """
            game_index = len(self.car_positions[car_id])-1
            if count == -1: count = len(self.car_positions[car_id])

            throttle = []
            for i in xrange(count):
                throttle.append(self.car_positions[car_id][game_index-i].throttle)
            return throttle

        #endregion

        # region getters for curent data

        def current_tick(self):
            return len(next(self.car_positions.itervalues())) - 1

        def current_piece(self, car_id):
            car_position = self.car_positions[car_id][self.current_tick()]
            index = car_position.piece_position.piece_index
            return self.track.piece_for_index(index)

        def current_piece_index(self, car_id):
            car_position = self.car_positions[car_id][self.current_tick()]
            return car_position.piece_position.piece_index

        def current_track_piece_index(self, car_id):
            return self.car_positions[car_id][self.current_tick()].piece_position.piece_index

        def current_in_piece_distance(self, car_id):
            return self.car_positions[car_id][self.current_tick()].piece_position.in_piece_distance

        def current_segment_length(self, car_id):
            car_position = self.car_positions[car_id][self.current_tick()]
            piece_index = car_position.piece_position.piece_index
            lane_index = car_position.piece_position.start_lane_index
            return self.track.length_of_current_segment(piece_index, lane_index)

        def current_start_lane_index(self, car_id):
            return self.car_positions[car_id][self.current_tick()].piece_position.start_lane_index

        def current_end_lane_index(self, car_id):
            return self.car_positions[car_id][self.current_tick()].piece_position.end_lane_index

        def current_segment(self, car_id):
            """
            Returns the current track segment
            """
            current_tick = self.current_tick()
            car_position = self.car_positions[car_id][current_tick]
            piece_index = car_position.piece_position.piece_index
            return self.track.segment_for_index(piece_index)

        def next_segment(self, car_id):
            """
            Returns the the next segment for given car and index where car is
            """
            current_segment = self.current_segment(car_id)
            return self.track.next_segment(current_segment)

        def distance_between_cars(self, c1, c2):
            # calculates the distance of c2 - c1
            current_tick = self.current_tick()
            p1 = self.car_positions[c1][current_tick]
            p2 = self.car_positions[c2][current_tick]

            if p1.piece_position.piece_index == p2.piece_position.piece_index:
                # They are on the same piece
                d = p2.piece_position.in_piece_distance - p1.piece_position.in_piece_distance
                return d

            # Get segments of each piece
            s1 = self.track.segment_for_index(p1.piece_position.piece_index)
            s2 = self.track.segment_for_index(p2.piece_position.piece_index)

            segment_delta = (s2.id - s1.id) % len(self.track.segments)

            lane_offset = self.track.left_lane_index(p2.piece_position.end_lane_index)
            d = 0
            if segment_delta > 1:
                # calc length between segments
                for s in range(s1.id+1, s2.id-1):
                    segment = self.track.segments[s]
                    d += segment.length(lane_offset)

            # Calc length on first and last segment
            d += s1.distance_to_next_segment(p1.piece_position.piece_index,
                                             p1.piece_position.in_piece_distance,
                                             lane_offset)
            d += s2.length(lane_offset) - s2.distance_to_next_segment(p2.piece_position.piece_index,
                                                                      p2.piece_position.in_piece_distance,
                                                                      lane_offset)
            return d

        #endregion

        #region Helper methods
        def export_recordings(self):
            filename = os.path.join("log", datetime.now().strftime("%Y%m%d_%H%M%S_RecordLog.csv"))
            with open(filename, "w") as csv_file:
                record_writer = csv.writer(csv_file, delimiter=',',
                                           quotechar='|', quoting=csv.QUOTE_MINIMAL)
                records = []
                for c in self.car_ids():
                    records.append(self.distance(c, -1))
                    records.append(self.angle(c, -1))
                    records.append(self.omega(c, -1))
                    records.append(self.velocity(c, -1))
                    records.append(self.throttle(c, -1))

                    s_lanes = []
                    e_lanes = []

                    for e in  self.car_positions[c]:
                        sl = e.piece_position.start_lane_index
                        el = e.piece_position.end_lane_index
                        s_lanes.append(sl)
                        e_lanes.append(el)
                    records.append(s_lanes)
                    records.append(e_lanes)

                for row in reversed(zip(*records)):
                    record_writer.writerow(row)
    #endregion
