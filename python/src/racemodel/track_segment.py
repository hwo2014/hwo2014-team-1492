
import math
import functools
import logging
from collections import OrderedDict
from track_piece import TrackPiece


# create sign function
sign = functools.partial(math.copysign, 1)


class TrackSegment(object):

    # Segment types
    NORMAL, BEND_N180_EX, BEND_N180, BEND_N90, \
    BEND_N45, BEND_P45, BEND_P90, BEND_P180, BEND_P180_EX = range(9)

    SEGMENT_NAMES = {
        NORMAL          : "Normal",
        BEND_P180_EX    : "Positive over 180",
        BEND_N180_EX    : "Negative over 180",
        BEND_P180       : "Positive LESS 180",
        BEND_N180       : "Negative LESS 180",
        BEND_P90        : "Positive LESS 90",
        BEND_N90        : "Negative LESS 90",
        BEND_P45        : "Positive LESS 45",
        BEND_N45        : "Negative LESS 45",
    }


    LEFT, RIGHT = True, False

    BEND_TYPES = [
        (None, -180, BEND_N180_EX, LEFT),
        (-180, -90, BEND_N180, LEFT),
        (-90, -45, BEND_N90, LEFT),
        (-45, 0, BEND_N45, LEFT),
        (0, 45, BEND_P45, RIGHT),
        (45, 90, BEND_P90, RIGHT),
        (90, 180, BEND_P180, RIGHT),
        (180, None, BEND_P180_EX, RIGHT)]

    def __init__(self, segment_id, piece=None, index=None):
        self.id = segment_id
        self.pieces = OrderedDict()
        self.type = None
        self.direction = None

        self.radius = 0
        self._angle = 0

        if piece:
            self.put_track_piece(piece, index)

    def __str__(self):
        msg = ""
        for k,v in self.pieces.iteritems():
            msg += "{0} - {1}\n".format(k, str(v))
        return "\nSegment {0}:\n".format(self.id) + msg

    def put_track_piece(self, piece, index):
        if self.type == None:
            # This is our first segment
            self._append_piece(index, piece)

            if piece.type in [TrackPiece.NORMAL, TrackPiece.SWITCH]:
                self.type = TrackSegment.NORMAL
            elif piece.type in [TrackPiece.BEND, TrackPiece.BENDSWITCH]:
                if piece.positive_angle:
                    self.type = TrackSegment.BEND_P45
                    self.direction = TrackSegment.RIGHT
                else:
                    self.type = TrackSegment.BEND_N45
                    self.direction = TrackSegment.LEFT

                self.radius = piece.radius
        else:
            # We already have pieces
            if self.type == TrackSegment.NORMAL:
                if piece.type not in [TrackPiece.NORMAL, TrackPiece.SWITCH]:
                    raise AttributeError
                else:
                    self._append_piece(index, piece)
            else:
                # It's some kind of bend
                if piece.type not in [TrackPiece.BEND, TrackPiece.BENDSWITCH]:
                    # Only accept bends
                    raise AttributeError

                last_piece_angle_sign = sign(self.angle_of_last_piece)
                current_piece_angle_sign = sign(piece.angle)

                if last_piece_angle_sign != current_piece_angle_sign:
                    # Bend in different direction
                    raise AttributeError

                if piece.radius != self.radius:
                    # Different radius
                    raise AttributeError

                # Segment is ok for segment. Add ait and calculate new type
                self._append_piece(index, piece)
                # Update bend type
                self.type = self.bend_type_for_angle()

    def bend_type_for_angle(self):
        bend_angle = self.angle
        for bend_type_definition in self.BEND_TYPES:
            lower_bound = bend_type_definition[0]
            upper_bound = bend_type_definition[1]
            bend_type = bend_type_definition[2]

            if lower_bound != None and upper_bound != None:
                if lower_bound < bend_angle <= upper_bound:
                    return bend_type
            elif lower_bound != None and upper_bound == None:
                if bend_angle > lower_bound:
                    return bend_type
            elif lower_bound == None and upper_bound != None:
                if bend_angle <= upper_bound:
                    return bend_type

        logging.error("Could not find proper bend type for angle {0}".format(bend_angle))
        assert False

    def available_indices(self):
        return list(self.pieces.iterkeys())

    def has_index(self, index):
        return index in self.available_indices()

    def piece_for_index(self, index):
        return self.pieces[index]

    def distance_to_next_piece(self, index, in_piece_distance, lane_radius_offset):
        p = self.pieces[index]
        # TODO: What about switch segment?
        return p.calc_length(lane_radius_offset) - in_piece_distance

    def distance_to_next_segment(self, index, in_piece_distance, lane_radius_offset):
        distance = self.distance_to_next_piece(index, in_piece_distance, lane_radius_offset)
        start_count = False
        for k, p in self.pieces.iteritems():
            if k == index:
                start_count = True
                continue
            if start_count:
                distance += p.calc_length(lane_radius_offset)
        return distance

    def length(self, lane_radius_offset):
        length = 0
        for p in self.pieces.itervalues():
            length += p.calc_length(lane_radius_offset)
        return length

    def merge_before(self, other_segment):
        """
        Merges all pieces of a another segment before the current one,
        """
        new_segment_dict = other_segment.pieces
        for k,v in self.pieces.iteritems():
            new_segment_dict[k] = v
        self.pieces = new_segment_dict

    def average_radius(self, lane_radius_offset=0):
        if self.type == TrackSegment.NORMAL:
            return 0
        radius = 0
        for p in self.pieces.itervalues():
            radius +=  p.effective_radius(lane_radius_offset)
        average_radius = radius / len(self.pieces)
        return average_radius

    def has_switch(self, index):
        """
        Returns whether this segment has a switch which is next to current index
        If so it returns the index of this switch element.
        Otherwise it returns -1
        """
        indices = self.available_indices()
        try:
            if index == indices[-1]:
                # index is last element of this segment
                return -1
            start = indices.index(index + 1)
        except ValueError:
            # Index is not this segment, look for all
            start = 0

        for piece_index in indices[start:]:
            piece = self.pieces[piece_index]
            if piece.type == TrackPiece.SWITCH or \
               piece.type == TrackPiece.BENDSWITCH:
                return piece_index
        # This segment does not have any switch element
        return -1

    # region properties

    @property
    def last_piece(self):
        """
        Returns the last added element to the ordered dict of pieces.
        """
        last_key =  next(reversed(self.pieces))
        return self.pieces[last_key]

    @property
    def angle_of_last_piece(self):
        """
        Returns the angle of last track piece
        """
        return self.last_piece.angle

    @property
    def angle(self):
        """
        Accessing the angle of this segment.
        """
        # TODO: howto deal with S-Bend
        return self._angle

    #endregion

    #region private helpers

    def _append_piece(self, index, piece):
        """
        Appends a track piece to the segment and updates the angle
        """
        self.pieces[index] = piece
        # TODO: Howto deal with S-bends?
        piece_angle = piece.angle if piece.angle else 0
        self._angle += piece_angle

    #endregion


