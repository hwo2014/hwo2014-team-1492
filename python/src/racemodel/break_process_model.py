

import math
import logging
from src.util.signal_processing import mean, median, variance, \
                                       find_element
from src.util.utils import abs_list


class BreakProcessModel(object):
    def __init__(self, race_model, race_data):
        self.race_model = race_model
        self.race_data = race_data
        self._process = []
        self._mean = 0
        self._variance = 0
        self._median = 0

         # Load initial values from database
        self.fetch_initial_coefficients()

    def append_break_process(self, b):
        self._process.append(b)

        # Determine non-NAN elements
        c = []
        for b in self._process:
            if not math.isnan(b.acceleration_coefficient):
                c.append(b.acceleration_coefficient)

        if len(c) == 0:
            # No valid values
            return

        self._mean = mean(c)
        self._variance = variance(c)
        self._median = median(c)

    #region DB connectors

    def fetch_initial_coefficients(self):
        same = self.race_data.acceleration_coefficients(self.race_model.track.id, "==")

        if len(same) > 0:
            # Use minimum to be on safe side
            _, idx = find_element(min, abs_list(same[1]))
            self._median = same[1][idx]
            self._mean = same[0][idx]
            logging.info("Found BreakProcess initial values from same track {:s} "
                         "Mean: {:.5f}, Median: {:.5f}".format(
                self.race_model.track.id,
                self.mean_acceleration_coefficient,
                self.acceleration_coefficient_median
            ))
            return

        other = self.race_data.acceleration_coefficients(self.race_model.track.id, "!=")

        if len(other) > 0:
            # Use minimum to be on safe side
            _, idx = find_element(min, abs_list(other[1]))
            self._median = other[1][idx]
            self._mean = other[0][idx]
            logging.info("Found BreakProcess initial values from other tracks "
                         "Mean: {:.5f}, Median: {:.5f}".format(
                self.mean_acceleration_coefficient,
                self.acceleration_coefficient_median
            ))
            return

        # Fallback if database is empty
        self._mean = -0.018707615500565693
        self._median = -0.018707615500565693


    def export_coefficients(self):
        """
        Export acceleration values to database
        """
        for b in self._process:
            logging.info(str(b))

        logging.info("Mean coefficient: {:.3f}, Variance {:.3f}, Median {:.3f}".format(
            self.mean_acceleration_coefficient,
            self.acceleration_coefficient_variance,
            self.acceleration_coefficient_median
        ))

        if self.mean_acceleration_coefficient == 0.0 or \
           self.acceleration_coefficient_median == 0.0:
            # Ignore outliers
            return

        self.race_data.put_acceleration_coefficient(
            self.race_model.track.id,
            self.mean_acceleration_coefficient,
            self.acceleration_coefficient_median
        )

    #endregion

    #region properties

    @property
    def mean_acceleration_coefficient(self):
        return self._mean

    @property
    def acceleration_coefficient_variance(self):
        return self._variance

    @property
    def acceleration_coefficient_median(self):
        return self._median

    @property
    def acceleration_coefficient(self):
        return self.acceleration_coefficient_median

    #endregion