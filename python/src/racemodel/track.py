from __future__ import division
import logging
from Queue import Queue, Empty

from track_segment import TrackSegment
from track_piece import TrackPiece


class Track(object):

    def __init__(self, json_track):
        self._current_index = 0
        self.name = json_track["name"]
        self.id = json_track["id"]
        self.segments = self._read_pieces(json_track["pieces"])
        self.lanes = self._read_lanes(json_track["lanes"])
        self.starting_point = self._read_starting_point(json_track["startingPoint"])
        logging.info("Parsed {0} segments".format(len(self.segments)))

        for s in self.segments:
            logging.info(str(s))

    def __str__(self):
        pieces = "\n".join(str(p) for p in self.segments)
        lanes = "\n".join(self.lanes)
        return "Name: {0}, Id: {1}\n" \
               "{2}\n" \
               "Lanes: " \
               "{3}\n" \
               "StartingPoint: {4}, {5}, {6}".format(self.name,
                                                     self.id,
                                                     pieces,
                                                     lanes,
                                                     *self.starting_point)
    def right_lane_index(self, lane_index):
        new_index = lane_index + 1
        return min(new_index, len(self.lanes)-1)

    def left_lane_index(self, lane_index):
        new_index = lane_index - 1
        return max(new_index, 0)

    def distance_to_next_piece(self, index, lane_index, in_piece_distance):
        index_segment = self.segment_for_index(index)
        lane_radius_offset = self.lane_for_index(lane_index)
        # TODO: What about switch here?
        return index_segment.distance_to_next_piece(index,
                                                    in_piece_distance,
                                                    lane_radius_offset)

    def distance_to_next_segment(self, index, lane_index, in_piece_distance):
        index_segment = self.segment_for_index(index)
        lane_radius_offset = self.lane_for_index(lane_index)
        return index_segment.distance_to_next_segment(index,
                                                      in_piece_distance,
                                                      lane_radius_offset)

    def length_of_current_segment(self, index, lane_index):
        index_segment = self.segment_for_index(index)
        lane_radius_offset = self.lane_for_index(lane_index)
        return index_segment.length(lane_radius_offset)

    def piece_for_index(self, index):
        for segment in self.segments:
            if segment.has_index(index):
                return segment.piece_for_index(index)
        logging.error("Piece for Index {0} not found".format(index))
        assert False

    def next_piece_index(self, piece_index):
        """
        Returns the next piece index for a given one
        """
        p = (piece_index + 1) % self.piece_cnt
        return p

    def piece_delta(self, p1, p2):
        """
        Returns the delta of pieces between.
        """
        return (p2 - p1) % self.piece_cnt

    def lane_for_index(self, index):
        return self.lanes[index]

    def segment_for_index(self, index):
        for s in self.segments:
            if s.has_index(index):
                return s
        logging.error("Segment for index {0}".format(index))
        assert False

    def max_normal_segment_len(self):
        maxlen = 0
        for seg in self.segments:
            if seg.type == TrackSegment.NORMAL:
                if seg.length(0) > maxlen:
                    maxlen = seg.length(0)
        return maxlen

    def next_segment(self, segment):
        current_index = self.segments.index(segment)
        return self.segments[(current_index + 1) % len(self.segments)]

    def next_bend(self, segment):
        """
        Returns the next bend segment
        """
        next_segment = segment
        while True:
            next_segment = self.next_segment(next_segment)
            if next_segment.angle:
                return next_segment

    def next_switch_index(self, segment, index):
        """
        Returns the next switch piece outgoing from current segment.
        """
        # TODO: Check if any segment has a switch
        next_segment = segment
        while True:
            switch_index = next_segment.has_switch(index)
            if switch_index != -1:
                return switch_index
            next_segment = self.next_segment(next_segment)

    def bends_until_next_switch(self, track_piece_index, angle=0):
        bend_segments = []
        segment = self.segment_for_index(track_piece_index)

        while True:
            if abs(segment.angle) >= angle:
                bend_segments.append(segment)
            if segment.has_switch(track_piece_index) != -1:
                return bend_segments

            track_piece_index = 0
            segment = self.next_segment(segment)


    #region Private Helpers

    @property
    def _next_segment_id(self):
        if not hasattr(self, "_next_segment_id_cnt"):
            self._next_segment_id_cnt = 0
        else:
            self._next_segment_id_cnt += 1
        return self._next_segment_id_cnt

    def _read_pieces(self, json_pieces):
        pieces = Queue()
        self.piece_cnt = 0
        for json_piece in json_pieces:
            length = json_piece.get("length", None)
            switch = json_piece.get("switch", None)
            radius = json_piece.get("radius", None)
            angle  = json_piece.get("angle",  None)
            pieces.put_nowait(TrackPiece(length, switch, radius, angle))
            self.piece_cnt += 1

        # All pieces are parsed into a single line, now put them
        # together into so called track segments.
        track_segments = []
        current_segment = TrackSegment(self._next_segment_id)
        while True:
            try:
                track_piece_to_process = pieces.get_nowait()
            except Empty:
                # We do not have pieces anymore.
                # Last segment and first segment might be of same type.
                # If so, merge them
                merge = False
                if track_segments[0].type == current_segment.type and \
                   track_segments[0].type == TrackSegment.NORMAL:
                    # Both segments are normal ones, merge them
                    merge = True
                elif track_segments[0].direction == current_segment.direction:
                    # Both are bends in same direction, merge them
                    merge = True

                if merge:
                    logging.debug("Merge segment to first one")
                    track_segments[0].merge_before(current_segment)
                else:
                    logging.debug("Do not merge last segment")
                    # They are not the same, therefore add the last segment
                    track_segments.append(current_segment)
                break

            try:
                current_segment.put_track_piece(track_piece_to_process,
                                                self._current_index)
                #logging.info("Adding piece {0} to segment".format(self._current_index))
                self._current_index += 1
            except AttributeError:
                # Current piece to process does not belong to current section
                # This means current section is finished. Add them to the
                # global segments and create a new segment.
                track_segments.append(current_segment)
                current_segment = TrackSegment(self._next_segment_id,
                                              track_piece_to_process,
                                               self._current_index)
                self._current_index += 1
        return track_segments

    def _read_lanes(self, lanes):
        l = [0] * len(lanes)
        for lane in lanes:
            index = int(lane["index"])
            l[index] = float(lane["distanceFromCenter"])
            logging.info("Lane: {0}, Distance {1}".format(index, l[index]))
        return l

    def _read_starting_point(self, starting_point):
        x = float(starting_point["position"]["x"])
        y = float(starting_point["position"]["y"])
        angle = float(starting_point["angle"])
        return x, y, angle


    # endregion

