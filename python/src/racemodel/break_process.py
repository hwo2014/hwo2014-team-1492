
import logging
from src.util.signal_processing import linear_regression, diff


class BreakProcess(object):
    def __init__(self, car_id, race_model, start_breakpoint, tick):
        self.id = car_id
        self.race_model = race_model
        self.start_breakpoint = start_breakpoint
        self.ticks = [tick]
        self.start_v = 0
        self.acceleration_coefficient = None

    def __str__(self):
        if self.acceleration_coefficient is not None:
            return "Acc Coeff: {:.5f}".format(
                self.acceleration_coefficient
            )
        else:
            return "nan because of no data"

    def end_break_point(self, tick):
        self.ticks.append(tick)

    def estimate_acceleration(self):
        if self.acceleration_coefficient is not None:
            return self.acceleration_coefficient

        assert len(self.ticks) == 2

        start = self.ticks[0]
        end = self.ticks[1]

        length = end - start + 1
        v = list(reversed(self.race_model.velocity(self.id, length)))

        if len(v) == 0:
            # we don't have any velocity data yet
            logging.info("Cannot estimate acceleration because of no velocity data")
            return

        self.start_v = v[0]

        if length < 5:
            logging.info("Cannot estimate acceleration model, too less values")
            self.acceleration_coefficient = float('nan')
            return

        logging.info("Length of v {}".format(len(v)))
        a = [0] + diff(v)

        v = v[2:-1]
        a = a[2:-1]

        logging.info("Length of v {}".format(len(v)))

        d, k = linear_regression(a, v)

        logging.info("Estimated acceleration coefficient:"
                     "k = {:.5f}, d = {:.5f}".format(k, d))
        self.start_v = v[0]
        self.acceleration_coefficient = k
        return k

    @property
    def v_break_start(self):
        return self.start_v
