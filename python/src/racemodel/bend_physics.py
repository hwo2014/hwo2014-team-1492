import math
import logging
from src.racemodel.track_segment import TrackSegment


class BendPhysicsEstimator(object):

    ARMED, NOT_ARMED = "Armed", "Not Armed"

    def __init__(self, car_id, race_model, race_data):
        self.id = car_id
        self.race_model = race_model
        self.race_data = race_data
        self.old_angle = 0.0
        self.state = BendPhysicsEstimator.NOT_ARMED
        self._a_slip = []

    def change_state(self, new_state):
        assert new_state in [BendPhysicsEstimator.NOT_ARMED, BendPhysicsEstimator.ARMED]
        logging.info("Changing BendPhysicsState from {} to {}".format(
            self.state, new_state
        ))
        self.state = new_state

    def update(self):
        if self.state == BendPhysicsEstimator.NOT_ARMED:
            # Do nothing if we don't have to
            return

        segment = self.race_model.current_segment(self.id)
        if segment.type == TrackSegment.NORMAL:
            logging.info("WTF???? We're trying to estimate slip factor on normal piece")
            self.change_state(BendPhysicsEstimator.NOT_ARMED)
            return

        angle = abs(self.race_model.angle(self.id)[0])

        if angle > 0.1 and self.old_angle <= 0.1:
            # We started slipping
            v = self.race_model.velocity(self.id)[0]
            # Determine current average radius
            # TODO: What to do with S-bends
            end_lane_index = self.race_model.current_end_lane_index(self.id)
            lane_offset = self.race_model.track.lane_for_index(end_lane_index)
            r = self.race_model.current_piece(self.id).effective_radius(lane_offset)

            logging.info("Input for bend estimation: "
                         "r: {:.1f}, v: {:.3f}, Angle: {:.5f}, Old Angle {:.5f}".format(
                r, v, angle, self.old_angle
            ))

            B = math.degrees(v / r)
            B_slip = B - angle
            v_thres = math.radians(B_slip) * r
            a_slip = v_thres ** 2 / r
            self._a_slip.append(a_slip)
            logging.info("Estimated new a_slip {:.5f}".format(a_slip))

            if len(self._a_slip) == 1:
                logging.info("Precalculating velocities")
                self.race_data.calculate_v_slip(a_slip)

            self.change_state(BendPhysicsEstimator.NOT_ARMED)

        self.old_angle = angle

    @property
    def a_slip(self):
        if len(self._a_slip):
            # TODO: maybe doing median
            return self._a_slip[0]
        else:
            return -1

    def test(self):
        r = 190
        v = 8.50
        a_new = 0.72399
        # a_old = 0.03651
        a_old = 0

        B = math.degrees(v / r)
        B_slip = B - (a_new - a_old)
        v_thres = math.radians(B_slip) * r
        a_slip = v_thres ** 2 / r
        self._a_slip.append(a_slip)
        self.change_state(BendPhysicsEstimator.NOT_ARMED)




