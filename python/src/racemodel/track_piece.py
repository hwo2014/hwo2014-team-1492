
import math
import logging


class TrackPiece(object):

    NORMAL, SWITCH, BEND, BENDSWITCH = range(4)

    def __init__(self, length=None, switch=None, radius=None, angle=None):
        self.length = None
        self.switch = None
        self.radius = None
        self.angle  = None
        self.positive_angle = False

        if length and not switch and not radius and not angle:
            self.type = self.NORMAL
            self.length = float(length)
        elif length and switch and not radius and not angle:
            self.type = self.SWITCH
            self.length = float(length)
            self.switch = switch
        elif not length and not switch and radius and angle:
            self.type = self.BEND
            self.radius = float(radius)
            self.angle  = float(angle)
            self.positive_angle = self.angle > 0
        elif not length and switch and radius and angle:
            self.type = self.BENDSWITCH
            self.switch = switch
            self.radius = float(radius)
            self.angle  = float(angle)
            self.positive_angle = self.angle > 0
        else:
            logging.error("Wrong Track Piece Parameters:\n"
                          "Length: {0}, "
                          "Switch: {1}, "
                          "Radius: {2}, "
                          "Angle:  {3}".format(length, switch, radius, angle))
            raise AttributeError("Wrong Track Piece Parameters")

    def effective_radius(self, center_distance):
        if self.angle < 0:
            r = self.radius + center_distance
        else:
            r = self.radius - center_distance
        return r

    def calc_length(self, center_distance):
        if self.type == self.NORMAL or self.type == self.SWITCH:
            return self.length
        else:
            r = self.effective_radius(center_distance)
            return abs(r * math.pi * self.angle / 180.0)

    def switch_length(self, lane_distance):
        assert lane_distance > 0
        if self.type == TrackPiece.NORMAL:
            return self.length
        elif self.type == TrackPiece.SWITCH:
            return math.sqrt(self.length**2 + lane_distance**2)
        else:
            r = self.effective_radius(0)
            l = abs(r * math.pi * self.angle / 180.0)
            return math.sqrt(l**2 + lane_distance**2)

    def __str__(self):
        if self.type == self.NORMAL:
            return "Track: NORMAL, Length: {:.1f}".format(self.length)
        elif self.type == self.SWITCH:
            return "Track: SWITCH, Length: {:.1f}".format(self.length)
        elif self.type == self.BEND:
            return "Track: BEND, Radius: {:.1f}, Angle {:.1f}".format(self.radius, self.angle)
        else:
            return "Track: BENDSWITCH, Radius: {:.1f}, Angle {:.1f}".format(self.radius, self.angle)
