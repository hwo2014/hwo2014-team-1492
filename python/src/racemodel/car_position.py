
import logging


class PiecePosition(object):
    def __init__(self, json_piece_position):
        self.piece_index = int(json_piece_position["pieceIndex"])
        self.in_piece_distance = float(json_piece_position["inPieceDistance"])
        self.start_lane_index = int(json_piece_position["lane"]["startLaneIndex"])
        self.end_lane_index = int(json_piece_position["lane"]["endLaneIndex"])
        self.lap = int(json_piece_position["lap"])

    def __str__(self):
        return "Piece Index:       {0}\n" \
               "In Piece Distance: {1}\n" \
               "Start Lane Index:  {2}\n" \
               "End Land Index:    {3}\n" \
               "Lap:               {4}".format(self.piece_index,
                                               self.in_piece_distance,
                                               self.start_lane_index,
                                               self.end_lane_index,
                                               self.lap)


class CarPosition(object):
    def __init__(self, json_data):
        self.id = json_data["id"]["name"], json_data["id"]["color"]
        self.angle = float(json_data["angle"])
        self.piece_position = PiecePosition(json_data["piecePosition"])
        self.throttle = 0

    def __str__(self):
        return "ID: {0}, Angle {1}" \
               "{2}".format(self.id, self.angle, str(self.piece_position))

