import logging

class LapInformation(object):
    def __init__(self):
        self.race_time = 0
        self.race_ticks = 0
        self.lap_time = []
        self.lap_ticks = []

    def add_lap(self, json_lap):
        self.lap_time.append(json_lap["lapTime"]["millis"])
        self.lap_ticks.append(json_lap["lapTime"]["ticks"])
        self.race_time = json_lap["raceTime"]["millis"]
        self.race_ticks = json_lap["raceTime"]["ticks"]
        logging.info("Current Lap Time: {0}".format(json_lap["lapTime"]["millis"] / 1000.0))

    @property
    def current_lap(self):
        return len(self.lap_time) + 1
