

import logging

class Car(object):
    def __init__(self, json_car):
        self.id = json_car["id"]["name"], json_car["id"]["color"]
        dimensions = json_car["dimensions"]
        self.length = dimensions["length"]
        self.width = dimensions["width"]
        self.flag_position = dimensions["guideFlagPosition"]

    def __str__(self):
        return "Name: {0} Color: {1} Length: {2} " \
               "Width: {3} Flag Position: {4}".format(self.id[0],
                                                      self.id[1],
                                                      self.length,
                                                      self.width,
                                                      self.flag_position)

class Cars(object):
    def __init__(self, json_cars):
        self.cars = self._read_cars(json_cars)

    def _read_cars(self,cars):
        c = []
        for car in cars:
            c.append(Car(car))
        return c

    def get_car_by_ID(self, id):
        for car in self.cars:
            if car.id == id:
                return car
        return None