import json
import socket
import sys
import logging
import os
from datetime import datetime

from src.bazingabot import BazingaBot

def configure_logger():
    log_file = os.path.join("log", datetime.now().strftime("%Y%m%d_%H%M%S_BazingaBot.log"))
    logging.basicConfig(format="%(asctime)s - %(levelname)s - %(message)s",
                        filename=log_file,
                        filemode="w")
    logging.getLogger().setLevel(logging.DEBUG)

    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


if __name__ == "__main__":
    # Configure logger
    configure_logger()

    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        logging.info("Connecting with parameters:")
        logging.info("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = BazingaBot(s, name, key)
        bot.run()
