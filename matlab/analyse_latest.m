close all
clear all

logs = dir('../python/log/*Log.csv');
filename = ['../python/log/' logs(size(logs, 1)).name];
data = csvread(filename);

distance  = data(:, 1)';
angular  = data(:, 2)';
angular_velocity  = data(:, 3)';
velocity  = data(:, 4)';
throttle = data(:, 5)' * 100;
acceleration = [0 diff(velocity)];

acceleration(acceleration < -30) = 0;
velocity(velocity > 20) = 0

start_lane_index = data(:,6)' * 50;
end_lane_index = data(:,7)' * 50;

start_tick = 1;
end_tick = length(data);
%
%start_tick = 1440
%end_tick = 1464;

tick = start_tick:end_tick;

figure;
plot(tick, angular(tick), '-r');
hold on;
plot(tick, angular_velocity(tick), '-g');
plot(tick, velocity(tick), '-k');
plot(tick, acceleration(tick), '-m');
%plot(tick, throttle, '-b');

%plot(start_lane_index, '--g')
%plot(end_lane_index, '--r')


grid on;
legend('Angular', 'Angular Velocity', ...
       'Velocity', 'Acceleration');

figure;
plot(velocity(tick), acceleration(tick))
xlabel('v')
ylabel('a')
xlim([0,7])

polyfit(velocity(tick), acceleration(tick), 1)


